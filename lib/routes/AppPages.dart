import 'package:get/get.dart';
import 'package:valorant/controller/agent_binding.dart';
import 'package:valorant/controller/maps_binding.dart';
import 'package:valorant/controller/on_boardinbg_binding.dart';
import 'package:valorant/controller/weapons_binding.dart';
import 'package:valorant/screens/agents/agent_detail_screen.dart';
import 'package:valorant/screens/agents/agent_screen.dart';
import 'package:valorant/screens/maps/maps_detail_screen.dart';
import 'package:valorant/screens/maps/maps_screen.dart';
import 'package:valorant/screens/on_boarding_screen.dart';
import 'package:valorant/screens/weapons/weapon_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.HOME,
        page: () => const OnBoardingScreen(),
        binding: OnBoardingBinding()),
    GetPage(
        name: AppRoutes.AGENTS,
        page: () => const AgentScreen(),
        binding: AgentBinding()),
    GetPage(
        name: AppRoutes.AGENT_DETAIL,
        page: () => const AgentDetailScreen(),
        binding: AgentBinding()),
    GetPage(
        name: AppRoutes.WEAPONS,
        page: () => const WeaponScreen(),
        binding: WeaponBinding()),
    GetPage(
        name: AppRoutes.MAPS,
        page: () => const MapsScreen(),
        binding: MapsBinding()),
    GetPage(
        name: AppRoutes.MAP_DETAIL,
        page: () => const MapDetailScreen(),
        binding: MapsBinding()),
  ];
}
