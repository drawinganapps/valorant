class AppRoutes {
  static const String HOME = '/';
  static const String AGENTS = '/agents';
  static const String AGENT_DETAIL = '/agent-detail';
  static const String WEAPONS = '/weapons';
  static const String MAPS = '/maps';
  static const String MAP_DETAIL = '/map-detail';
}
