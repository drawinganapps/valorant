import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:valorant/controller/maps_controller.dart';
import 'package:valorant/widgets/maps/map_detail_description.dart';
import 'package:valorant/widgets/maps/map_detail_information_widget.dart';
import 'package:valorant/widgets/maps/maps_gallery_widget.dart';

class MapsDetailPage extends StatelessWidget {
  const MapsDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MapsController>(builder: (controller) {
      return Column(
        children: [
          const MapDetailInformationWidget(),
          Expanded(
              child: ListView(
            physics: const BouncingScrollPhysics(),
            children: [
              MapDetailDescription(mapModel: controller.selectedMap),
              Container(
                margin: EdgeInsets.only(top: Get.height * 0.03),
                child: MapsGalleryWidget(mapModel: controller.selectedMap),
              )
            ],
          )),
        ],
      );
    });
  }
}
