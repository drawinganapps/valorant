import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:valorant/controller/maps_controller.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/routes/AppRoutes.dart';
import 'package:valorant/widgets/maps/maps_card_widget.dart';
import 'package:valorant/widgets/shared/default_loading_widget.dart';

class MapsPage extends StatelessWidget {
  const MapsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MapsController>(builder: (controller) {
      return Obx(() {
        return controller.isLoading
            ? const DefaultLoadingWidget()
            : Column(
                children: [
                  SizedBox(
                    child: Container(
                      margin:
                          EdgeInsets.only(bottom: Get.height * 0.02, top: 15),
                      width: Get.width * 0.5,
                      height: Get.height * 0.05,
                      decoration: BoxDecoration(
                          color: ColorHelper.primary.withOpacity(0.5),
                          borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(50),
                            bottomRight: Radius.circular(50),
                          )),
                      child: Center(
                        child: Text('MAPS',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                color: ColorHelper.white)),
                      ),
                    ),
                  ),
                  Expanded(
                      child: RefreshIndicator(
                        color: ColorHelper.white,
                        backgroundColor: ColorHelper.dark.withOpacity(0.2),
                        onRefresh: () async {
                          controller.refreshData();
                        },
                        child: ListView(
                          physics: const BouncingScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          children:
                          List.generate(controller.getAllMaps.length, (index) {
                            return GestureDetector(
                              onTap: () {
                                context.loaderOverlay.show(widget: const DefaultLoadingWidget());
                                controller.setSelectedMap(controller.getAllMaps[index]);
                                Future.delayed(const Duration(milliseconds: 500), () {
                                  Get.toNamed(AppRoutes.MAP_DETAIL);
                                  context.loaderOverlay.hide();
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.only(
                                    left: Get.width * 0.03, right: Get.width * 0.03),
                                margin: EdgeInsets.only(bottom: Get.height * 0.02),
                                child: MapsCardWidget(
                                    mapModel: controller.getAllMaps[index]),
                              ),
                            );
                          }),
                        ),
                      ))
                ],
              );
      });
    });
  }
}
