import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:valorant/widgets/agents/agent_detail_ability_widget.dart';
import 'package:valorant/widgets/agents/agent_detail_biograhpy_widget.dart';
import 'package:valorant/widgets/agents/agent_detail_information_widget.dart';

class AgentDetailPage extends StatelessWidget {
  const AgentDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const AgentDetailInformationWidget(),
        Expanded(
            child: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            const AgentDetailBiographyWidget(),
            Container(
              margin: EdgeInsets.only(top: Get.height * 0.03),
              child: const AgentDetailAbilityWidget(),
            )
          ],
        ))
      ],
    );
  }
}
