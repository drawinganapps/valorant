import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:valorant/controller/agent_controller.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/agent_model.dart';
import 'package:valorant/routes/AppRoutes.dart';
import 'package:valorant/widgets/agents/agent_card_widget.dart';
import 'package:valorant/widgets/shared/default_loading_widget.dart';
import 'package:valorant/widgets/shared/filter_widget.dart';

class AgentsPage extends StatelessWidget {
  const AgentsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AgentController>(builder: (controller) {
      const roleFilterValues = AgentRole.values;
      final screenWidth = Get.width;
      return Obx(() {
        return controller.isLoading
            ? const DefaultLoadingWidget()
            : Column(
                children: [
                  SizedBox(
                    height: 80,
                    child: ListView(
                      physics: const BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      children: List.generate(roleFilterValues.length, (index) {
                        return GestureDetector(
                          onTap: () {
                            controller
                                .changeFilterAgent(roleFilterValues[index]);
                          },
                          child: FilterWidget(
                            isSelected: roleFilterValues[index] ==
                                controller.selectedFilter,
                            filterName: EnumToString.convertToString(
                                roleFilterValues[index]),
                            color: ColorHelper.getBackgroundColorAgent(
                                roleFilterValues[index]),
                          ),
                        );
                      }),
                    ),
                  ),
                  Expanded(
                    child: RefreshIndicator(
                      onRefresh: () async {
                        controller.refreshData();
                      },
                      color: ColorHelper.white,
                      backgroundColor: ColorHelper.dark.withOpacity(0.2),
                      child: GridView.count(
                          padding: EdgeInsets.only(
                              left: screenWidth * 0.06,
                              right: screenWidth * 0.06),
                          crossAxisCount: 2,
                          crossAxisSpacing: 20,
                          mainAxisSpacing: 20,
                          childAspectRatio: 0.7,
                          physics: const BouncingScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          children: List.generate(
                              controller.filteredAgentList.length, (index) {
                            return GestureDetector(
                              onTap: () {
                                context.loaderOverlay
                                    .show(widget: const DefaultLoadingWidget());
                                Future.delayed(
                                    const Duration(milliseconds: 500), () {
                                  controller.setSelectedAgent(
                                      controller.filteredAgentList[index]);
                                  Get.toNamed(AppRoutes.AGENT_DETAIL);
                                  context.loaderOverlay.hide();
                                });
                              },
                              child: AgentCardWidget(
                                  agent: controller.filteredAgentList[index]),
                            );
                          })),
                    ),
                  ),
                ],
              );
      });
    });
  }
}
