import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:valorant/controller/weapon_controller.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/widgets/shared/default_loading_widget.dart';
import 'package:valorant/widgets/shared/filter_widget.dart';
import 'package:valorant/widgets/weapons/weapon_card_widget.dart';

class WeaponPage extends StatelessWidget {
  const WeaponPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<WeaponController>(builder: (controller) {
      final screenWidth = Get.width;
      return Obx(() {
        return controller.isLoading
            ? const DefaultLoadingWidget()
            : Column(
                children: [
                  SizedBox(
                    height: 80,
                    child: ListView(
                      physics: const BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      children: List.generate(
                          controller.getWeaponCategories.length, (index) {
                        return GestureDetector(
                          onTap: () {
                            controller.changeSelectedWeaponCategory(
                                controller.getWeaponCategories[index]);
                          },
                          child: FilterWidget(
                            isSelected: controller.getSelectedWeaponCategory ==
                                controller.getWeaponCategories[index],
                            filterName: controller
                                .getWeaponCategories[index].weaponCategoryName!,
                            color: ColorHelper.primary,
                          ),
                        );
                      }),
                    ),
                  ),
                  Expanded(
                    child: RefreshIndicator(
                      color: ColorHelper.white,
                      backgroundColor: ColorHelper.dark.withOpacity(0.2),
                      onRefresh: () async {
                        controller.refreshData();
                      },
                      child: GridView.count(
                          padding: EdgeInsets.only(
                              left: screenWidth * 0.06,
                              right: screenWidth * 0.06),
                          crossAxisCount: 2,
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10,
                          childAspectRatio: 1.3,
                          physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                          children: List.generate(
                              controller.getFilteredWeapons.length, (index) {
                            return WeaponCardWidget(
                                weapon: controller.getFilteredWeapons[index]);
                          })),
                    ),
                  ),
                ],
              );
      });
    });
  }
}
