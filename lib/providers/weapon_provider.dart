import 'package:get/get.dart';

class WeaponProvider extends GetConnect {
  // Get request
  Future<Response> getAllWeapons() async {
    Response<dynamic> response = await get('https://valorant-gateway-service.vercel.app/weapons');
    return response;
  }
}
