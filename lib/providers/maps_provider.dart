import 'package:get/get.dart';

class MapsProvider extends GetConnect {
  // Get request
  Future<Response> getAllMaps() async {
    Response<dynamic> response = await get('https://valorant-gateway-service.vercel.app/maps');
    return response;
  }
}
