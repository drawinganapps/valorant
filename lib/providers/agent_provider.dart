import 'package:get/get.dart';

class AgentProvider extends GetConnect {
  // Get request
  Future<Response> getAllAgents() async {
    Response<dynamic> response = await get('https://valorant-gateway-service.vercel.app/agents');
    return response;
  }
}
