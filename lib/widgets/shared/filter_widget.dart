import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/agent_model.dart';

class FilterWidget extends StatelessWidget {
  final bool isSelected;
  final String filterName;
  final Color color;

  const FilterWidget(
      {Key? key, required this.isSelected, required this.filterName, required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = Get.width;
    return Center(
      child: Container(
        margin: EdgeInsets.only(
            left: screenWidth * 0.06, right: screenWidth * 0.06),
        child: Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.black.withOpacity(0.8),
              boxShadow: isSelected ? <BoxShadow>[
                BoxShadow(
                    color: color.withOpacity(0.5),
                    blurRadius: 7,
                    spreadRadius: 3,
                    offset: const Offset(0.0, 0.75))
              ] : []),
          child: Row(
            children: [
              Text(filterName == 'All' ? 'Show All' : filterName,
                  style: GoogleFonts.roboto(
                      fontSize: 15,
                      color: ColorHelper.white,
                      fontWeight: FontWeight.w700)),
              Container(
                height: 20,
                width: 20,
                margin: const EdgeInsets.only(left: 5),
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color:color, width: 2)),
                child: isSelected
                    ? Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: color),
                      )
                    : Container(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
