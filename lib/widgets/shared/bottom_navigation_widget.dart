import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/routes/AppRoutes.dart';

class BottomNavigationWidget extends StatelessWidget {
  const BottomNavigationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = Get.width;
    return Container(
      padding:
          EdgeInsets.only(left: screenWidth * 0.09, right: screenWidth * 0.09),
      decoration: BoxDecoration(color: ColorHelper.dark),
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              padding: const EdgeInsets.all(0),
              child: GestureDetector(
                onTap: () {
                  Get.toNamed(AppRoutes.AGENTS);
                },
                child: Icon(
                  Icons.people_alt,
                  color: Get.currentRoute == AppRoutes.AGENTS
                      ? ColorHelper.primary
                      : ColorHelper.whiteDarker,
                  size: 35,
                ),
              )),
          Container(
              padding: const EdgeInsets.all(0),
              child: GestureDetector(
                onTap: () {
                  Get.toNamed(AppRoutes.WEAPONS);
                },
                child: Icon(
                  Icons.colorize,
                  color: Get.currentRoute == AppRoutes.WEAPONS
                      ? ColorHelper.primary
                      : ColorHelper.whiteDarker,
                  size: 35,
                ),
              )),
          Container(
            padding: const EdgeInsets.all(0),
            child: GestureDetector(
              onTap: () {
                Get.toNamed(AppRoutes.MAPS);
              },
              child: Icon(
                Icons.map,
                color: Get.currentRoute == AppRoutes.MAPS
                    ? ColorHelper.primary
                    : ColorHelper.whiteDarker,
                size: 35,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
