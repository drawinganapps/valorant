import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/routes/AppRoutes.dart';

class SideMenuDrawerWidget extends StatelessWidget {
  const SideMenuDrawerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget generateMenuWidget(String title, bool isSelected) {
      return Container(
        padding: const EdgeInsets.only(left: 25, right: 10, top: 5, bottom: 5),
        width: 50,
        decoration: isSelected
            ? BoxDecoration(
                color: ColorHelper.primary.withOpacity(0.5),
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50)))
            : const BoxDecoration(),
        child: Text(title,
            style: GoogleFonts.roboto(
                fontSize: 22,
                color: ColorHelper.white,
                fontWeight: FontWeight.bold)),
      );
    }

    return Drawer(
      backgroundColor: ColorHelper.dark,
      child: Container(
        padding: const EdgeInsets.only(top: 30),
        child: Column(
          // Important: Remove any padding from the ListView.
          children: <Widget>[
            Image.asset(
              'assets/logo/logo.png',
              width: 50,
              height: 50,
              fit: BoxFit.cover,
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 35),
              child: Text('Valorant',
                  style: TextStyle(fontSize: 25, color: ColorHelper.white)),
            ),
            ListTile(
              title: generateMenuWidget(
                  'Agents', Get.currentRoute == AppRoutes.AGENTS),
              onTap: () {
                Get.toNamed(AppRoutes.AGENTS);
              },
            ),
            ListTile(
              title: generateMenuWidget(
                  'Weapons', Get.currentRoute == AppRoutes.WEAPONS),
              onTap: () {
                Get.toNamed(AppRoutes.WEAPONS);
              },
            ),
            ListTile(
              title: generateMenuWidget(
                  'Maps', Get.currentRoute == AppRoutes.MAPS),
              onTap: () {
                Get.toNamed(AppRoutes.MAPS);
              },
            ),
            ListTile(
              title: generateMenuWidget('About', false),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
