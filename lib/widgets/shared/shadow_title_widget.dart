import 'package:flutter/material.dart';
import 'package:valorant/helper/color_helper.dart';

class ShadowTitleWidget extends StatelessWidget {
  final String text;
  const ShadowTitleWidget({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('// $text',
        maxLines: 1,
        overflow: TextOverflow.fade,
        style: TextStyle(
        fontSize: 18,
        color: ColorHelper.white,
        shadows: [
          Shadow(
              color: ColorHelper.white.withOpacity(0.9),
              blurRadius: 15,
              offset: const Offset(0.0, 0.75)
          )
        ]
    ));
  }

}
