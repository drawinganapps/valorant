import 'package:flutter/material.dart';
import 'package:valorant/helper/color_helper.dart';

class SelectedWidget extends StatelessWidget {
  final bool isSelected;

  const SelectedWidget({Key? key, required this.isSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Decoration unselectedDecoration = BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: ColorHelper.primary, width: 1));

    final Decoration selectedDecoration = BoxDecoration(
        shape: BoxShape.circle,
        color: ColorHelper.primary,
        border: Border.all(color: ColorHelper.primary, width: 1));

    return Container(
      padding: const EdgeInsets.all(6),
      decoration: isSelected ? selectedDecoration : unselectedDecoration,
    );
  }
}
