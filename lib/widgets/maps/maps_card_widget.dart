import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/map_model.dart';
import 'package:valorant/widgets/shared/shadow_title_widget.dart';

class MapsCardWidget extends StatelessWidget {
  final MapModel mapModel;

  const MapsCardWidget({Key? key, required this.mapModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * 0.25,
      decoration: BoxDecoration(
        image: DecorationImage(image: NetworkImage(mapModel.mapFeaturedImage!.url!), fit: BoxFit.cover, opacity: 1),
        color: Colors.white.withOpacity(0.5),
        borderRadius: BorderRadius.circular(20),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: ColorHelper.white.withOpacity(0.6),
              blurRadius: 1,
              spreadRadius: 0.2,
              offset: const Offset(0.0, 0.75))
        ]
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: Get.height * 0.06,
            width: Get.width * 0.55,
            decoration: BoxDecoration(
              color: ColorHelper.dark.withOpacity(0.6),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(50),
                bottomRight: Radius.circular(50),
              )
            ),
            child: Center(
              child: Text(mapModel.mapName!, style: TextStyle(
                  color: ColorHelper.white,
                  fontSize: 28,
                  fontWeight: FontWeight.bold
              )),
            ),
          )
        ],
      ),
    );
  }
}
