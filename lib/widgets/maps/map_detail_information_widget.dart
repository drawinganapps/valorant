import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:valorant/controller/maps_controller.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/map_model.dart';

class MapDetailInformationWidget extends StatelessWidget {
  const MapDetailInformationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final MapModel map = Get.find<MapsController>().selectedMap;
    final screenHeight = Get.height;
    final screenWidth = Get.height;
    return Container(
      padding: EdgeInsets.only(left: screenWidth * 0.03),
      height: screenHeight * 0.3,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(map.mapFeaturedImage!.url!),
              fit: BoxFit.cover),
          color: ColorHelper.white.withOpacity(0.2)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 45, bottom: 35),
                child: GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: ColorHelper.white.withOpacity(0.9),
                      size: 25,
                    )),
              ),
              Container(
                width: screenWidth * 0.3,
                height: screenHeight * 0.05,
                decoration: BoxDecoration(
                    color: ColorHelper.dark.withOpacity(0.6),
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(50),
                      bottomRight: Radius.circular(50),
                    )),
                child: Center(
                  child: Text(map.mapName!,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                          color: ColorHelper.white)),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
