import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/map_model.dart';
import 'package:valorant/widgets/shared/shadow_title_widget.dart';

class MapsGalleryWidget extends StatelessWidget {
  final MapModel mapModel;

  const MapsGalleryWidget({Key? key, required this.mapModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenHeight = Get.height;
    final screenWidth = Get.height;
    return Container(
      padding:
          EdgeInsets.only(left: screenWidth * 0.03, right: screenWidth * 0.03),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const ShadowTitleWidget(text: 'ALl Gallery'),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: Column(
              children: List.generate(mapModel.galleryImages!.length, (index) {
                return Container(
                  padding: EdgeInsets.all(screenWidth * 0.009),
                  margin: EdgeInsets.only(bottom: screenHeight * 0.015),
                  height: screenHeight * 0.25,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      image: DecorationImage(image: NetworkImage(mapModel.galleryImages![index].mapImage!.url!), fit: BoxFit.cover),
                      color: ColorHelper.lightDark),
                );
              }),
            ),
          )
        ],
      ),
    );
  }
}
