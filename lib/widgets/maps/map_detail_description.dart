import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/map_model.dart';
import 'package:valorant/widgets/shared/shadow_title_widget.dart';

class MapDetailDescription extends StatelessWidget {
  final MapModel mapModel;

  const MapDetailDescription({Key? key, required this.mapModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenHeight = Get.height;
    final screenWidth = Get.height;
    return Container(
      padding:
      EdgeInsets.only(left: screenWidth * 0.03, right: screenWidth * 0.03),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const ShadowTitleWidget(text: 'Description'),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: Text(mapModel.mapDescription!,
                textAlign: TextAlign.justify,
                style: GoogleFonts.roboto(
                    color: ColorHelper.white.withOpacity(0.6),
                    fontSize: 16,
                    fontWeight: FontWeight.w400)),
          )
        ],
      ),
    );
  }

}
