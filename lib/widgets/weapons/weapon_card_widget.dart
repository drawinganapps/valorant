import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/weapon_model.dart';

class WeaponCardWidget extends StatefulWidget {
  final WeaponModel weapon;

  const WeaponCardWidget({Key? key, required this.weapon}) : super(key: key);

  @override
  State<WeaponCardWidget> createState() => _WeaponCardWidget();
}

class _WeaponCardWidget extends State<WeaponCardWidget> {
  late bool _isReveled;

  @override
  void initState() {
    super.initState();
    _isReveled = false;
  }

  @override
  Widget build(BuildContext context) {
    final Widget revealedWidget = Container(
      width: Get.width,
      padding: EdgeInsets.only(
          left: Get.width * 0.02,
          right: Get.width * 0.02,
          top: Get.height * 0.01),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          image: DecorationImage(
              image: NetworkImage(widget.weapon.weaponAsset!.url!),
              opacity: 0.1,
              fit: BoxFit.cover),
          color: ColorHelper.primary),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.weapon.weaponName!,
              style: TextStyle(
                  color: ColorHelper.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold)),
          Text(widget.weapon.weaponHoverDescription![0],
              textAlign: TextAlign.justify,
              style: GoogleFonts.roboto(
                  color: ColorHelper.white.withOpacity(0.8),
                  fontWeight: FontWeight.bold,
                  fontSize: 11)),
        ],
      ),
    );

    final Widget unRevealedWidget = Container(
      width: Get.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white.withOpacity(0.3)),
      child: Column(
        children: [
          Stack(
            children: [
              Container(
                margin: const EdgeInsets.only(top: 15, left: 10),
                child: Image.network(
                  widget.weapon.weaponAsset!.url!,
                  color: Colors.black.withOpacity(0.1),
                  colorBlendMode: BlendMode.modulate,
                ),
              ),
              Image.network(widget.weapon.weaponAsset!.url!),
            ],
          )
        ],
      ),
    );

    return GestureDetector(
      onTap: () {
        setState(() {
          _isReveled = !_isReveled;
        });
      },
      child: Stack(
        children: [
          _isReveled ? revealedWidget : unRevealedWidget,
          _isReveled
              ? Container()
              : Positioned(
                  child: Container(
                  padding:
                      const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.weapon.weaponName!,
                          style: TextStyle(
                              color: ColorHelper.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold)),
                      Text(
                          widget.weapon.weaponCategoryMachineName!
                              .toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: ColorHelper.white, fontSize: 13)),
                    ],
                  ),
                ))
        ],
      ),
    );
  }
}
