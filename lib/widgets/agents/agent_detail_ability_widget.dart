import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:valorant/controller/agent_controller.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/agent_model.dart';
import 'package:valorant/widgets/shared/shadow_title_widget.dart';
import 'package:valorant/widgets/shared/video_player_widget.dart';

class AgentDetailAbilityWidget extends StatelessWidget {
  const AgentDetailAbilityWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AgentLocalizationModel localization =
        Get.find<AgentController>().agentLocalization;
    final AgentModel agent = Get.find<AgentController>().selectedAgent;
    final screenHeight = Get.height;
    final screenWidth = Get.height;
    return Container(
      padding:
          EdgeInsets.only(left: screenWidth * 0.03, right: screenWidth * 0.03),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ShadowTitleWidget(text: localization.abilitiesLabel!),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: Column(
              children: List.generate(agent.abilities!.length, (index) {
                return Container(
                  padding: EdgeInsets.all(screenWidth * 0.009),
                  margin: EdgeInsets.only(bottom: screenHeight * 0.015),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: ColorHelper.lightDark),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: screenWidth * 0.06,
                            margin: EdgeInsets.only(right: screenWidth * 0.02),
                            padding: EdgeInsets.all(screenWidth * 0.008),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: ColorHelper.lightDarkSecondary),
                            child: Image.network(
                              agent.abilities![index].abilityIcon!.url!,
                              height: screenHeight * 0.05,
                            ),
                          ),
                          SizedBox(
                            width: screenWidth * 0.28,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(agent.abilities![index].abilityName!,
                                    style: GoogleFonts.roboto(
                                        color: ColorHelper.white,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16)),
                                Container(
                                  margin: const EdgeInsets.only(top: 5),
                                  child: Text(
                                      agent.abilities![index]
                                          .abilityDescription!,
                                      textAlign: TextAlign.justify,
                                      overflow: TextOverflow.fade,
                                      style: GoogleFonts.roboto(
                                          color: ColorHelper.white
                                              .withOpacity(0.5),
                                          fontWeight: FontWeight.w400,
                                          fontSize: 13)),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: screenHeight * 0.01),
                        decoration: BoxDecoration(
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ColorHelper.lightDarkSecondary,
                                  blurRadius: 7,
                                  spreadRadius: 3,
                                  offset: const Offset(0.0, 0.75))
                            ],
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                                color: ColorHelper.lightDarkSecondary,
                                width: 1)),
                        clipBehavior: Clip.antiAlias,
                        child: VideoApp(
                            videoUrl: agent.abilities![index].abilityVideo![0]
                                .video!.file!.url!),
                      )
                    ],
                  ),
                );
              }),
            ),
          )
        ],
      ),
    );
  }
}
