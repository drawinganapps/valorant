import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/agent_model.dart';

class AgentCardWidget extends StatelessWidget {
  final AgentModel agent;

  const AgentCardWidget({Key? key, required this.agent}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = Get.width;
    return Stack(
      children: [
        Container(
          width: Get.width,
          margin: EdgeInsets.only(top: height * 0.09),
          child: Container(
            padding: const EdgeInsets.only(left: 15, right: 15),
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    bottomLeft: Radius.circular(15),
                    topRight: Radius.circular(70)),
                color: ColorHelper.getBackgroundColorAgent(
                        EnumToString.fromString(AgentRole.values, agent.role!)!)
                    .withOpacity(0.8),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: ColorHelper.getBackgroundColorAgent(
                              EnumToString.fromString(
                                  AgentRole.values, agent.role!)!)
                          .withOpacity(0.6),
                      blurRadius: 7,
                      spreadRadius: 3,
                      offset: const Offset(0.0, 0.75))
                ]),
          ),
        ),
        Positioned(
            child: Image.network(agent.agentImage!.url!, fit: BoxFit.fill)),
        Positioned(
            child: Container(
          padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(agent.title!,
                  style: TextStyle(
                      color: ColorHelper.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold)),
              Row(
                children: [
                  Image.network(
                    agent.roleIcon!.url!,
                    width: 15,
                    height: 15,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 5),
                  ),
                  Text(agent.role!.toUpperCase(),
                      style: GoogleFonts.roboto(
                          color: ColorHelper.white, fontSize: 13)),
                ],
              )
            ],
          ),
        ))
      ],
    );
  }
}
