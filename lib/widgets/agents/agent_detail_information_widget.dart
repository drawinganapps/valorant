import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:valorant/controller/agent_controller.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/agent_model.dart';

class AgentDetailInformationWidget extends StatelessWidget {
  const AgentDetailInformationWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final AgentModel agent = Get.find<AgentController>().selectedAgent;
    final screenHeight = Get.height;
    final screenWidth = Get.height;
    return Container(
      padding: EdgeInsets.only(left: screenWidth * 0.03),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(agent.agentImage!.url!),
              fit: BoxFit.cover,
              opacity: 0.5),
          color: ColorHelper.getBackgroundColorAgent(
              EnumToString.fromString(AgentRole.values, agent.role!)!)
              .withOpacity(0.2)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 45, bottom: 35),
                child: GestureDetector(
                    onTap: (){
                      Get.back();
                    },
                    child: Icon(Icons.arrow_back_ios, color: ColorHelper.white.withOpacity(0.9), size: 25,)),
              ),
              Text(agent.title!,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: ColorHelper.white)),
              Container(
                padding: const EdgeInsets.all(10),
                margin: const EdgeInsets.only(top: 10),
                decoration: BoxDecoration(
                    color: ColorHelper.dark.withOpacity(0.8),
                    borderRadius: BorderRadius.circular(10)),
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: Image.network(agent.roleIcon!.url!,
                          width: 15, height: 15),
                    ),
                    Text(agent.role!.toUpperCase(),
                        style: GoogleFonts.roboto(
                            fontWeight: FontWeight.w400,
                            color: ColorHelper.white,
                            fontSize: 16))
                  ],
                ),
              )
            ],
          ),
          Image.network(agent.agentImage!.url!,
              height: screenHeight * 0.35, fit: BoxFit.cover)
        ],
      ),
    );
  }

}
