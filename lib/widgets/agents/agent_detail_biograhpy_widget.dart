import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:valorant/controller/agent_controller.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/agent_model.dart';
import 'package:valorant/widgets/shared/shadow_title_widget.dart';

class AgentDetailBiographyWidget extends StatelessWidget {
  const AgentDetailBiographyWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AgentLocalizationModel localization =
        Get.find<AgentController>().agentLocalization;
    final AgentModel agent = Get.find<AgentController>().selectedAgent;
    final screenHeight = Get.height;
    final screenWidth = Get.height;
    return Container(
      padding:
          EdgeInsets.only(left: screenWidth * 0.03, right: screenWidth * 0.03),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ShadowTitleWidget(text: localization.biographyLabel!),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: Text(agent.description!,
                textAlign: TextAlign.justify,
                style: GoogleFonts.roboto(
                    color: ColorHelper.white.withOpacity(0.6),
                    fontSize: 16,
                    fontWeight: FontWeight.w400)),
          )
        ],
      ),
    );
  }
}
