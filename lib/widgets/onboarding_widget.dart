import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:valorant/controller/on_boarding_controller.dart';
import 'package:valorant/helper/color_helper.dart';
import 'package:valorant/models/on_boarding_model.dart';
import 'package:valorant/routes/AppRoutes.dart';
import 'package:valorant/widgets/shared/selected_widget.dart';

class OnBoardingWidget extends StatelessWidget {
  final OnBoardingModel onBoarding;

  const OnBoardingWidget({Key? key, required this.onBoarding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenHeight = Get.height;
    final screenWidth = Get.height;
    final controller = Get.find<OnBoardingController>();
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/img/${onBoarding.background}'),
              fit: BoxFit.cover)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          GestureDetector(
            onHorizontalDragEnd: (details) {
              if (details.primaryVelocity?.compareTo(0) == -1) {
                if (controller.currentIndex < 2) {
                  controller.changeObBoarding(controller.currentIndex += 1);
                }
              } else {
                if (controller.currentIndex > 0) {
                  controller.changeObBoarding(controller.currentIndex -= 1);
                }
              }
            },
            child: Container(
              width: screenWidth,
              height: screenHeight * 0.30,
              padding: const EdgeInsets.only(left: 15, right: 25, top: 15),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25))),
              child: Column(
                children: [
                  Text(onBoarding.title,
                      style: TextStyle(
                          fontSize: 26,
                          fontWeight: FontWeight.w500,
                          color: ColorHelper.primary)),
                  Container(
                    margin: const EdgeInsets.only(top: 10, bottom: 5),
                    child: Text(onBoarding.text,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            fontSize: 18, color: Colors.black)),
                  ),
                  Expanded(
                      child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: 80,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: List.generate(3, (index) {
                            return SelectedWidget(
                                isSelected: index == controller.currentIndex);
                          }),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: ColorHelper.primary,
                            borderRadius: BorderRadius.circular(15)),
                        child: IconButton(
                          onPressed: () {
                            if (controller.currentIndex < 2) {
                              controller.changeObBoarding(
                                  controller.currentIndex += 1);
                              return;
                            }
                            Get.toNamed(AppRoutes.AGENTS);
                          },
                          icon: Icon(Icons.navigate_next,
                              color: ColorHelper.white, size: 25),
                        ),
                      )
                    ],
                  ))
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
