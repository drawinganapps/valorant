import 'package:flutter/material.dart';
import 'DarkTheme.dart';

class AppTheme {
  static ThemeData dark = darkTheme;
}
