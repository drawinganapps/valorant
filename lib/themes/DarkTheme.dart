import 'package:flutter/material.dart';
import 'package:valorant/helper/color_helper.dart';

ThemeData darkTheme = ThemeData(
    brightness: Brightness.light,
    scaffoldBackgroundColor: ColorHelper.dark,
    highlightColor: Colors.transparent,
    splashColor: Colors.transparent,
    fontFamily: 'Valorant'
);
