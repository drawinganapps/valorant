import 'package:get/get.dart';
import 'package:valorant/providers/weapon_provider.dart';

class WeaponRepository extends GetxService {
  final WeaponProvider weaponProvider;

  WeaponRepository({required this.weaponProvider});

  Future<Response> getAllWeapons() async {
    return await weaponProvider.getAllWeapons();
  }
}
