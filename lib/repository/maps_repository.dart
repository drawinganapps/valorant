import 'package:get/get.dart';
import 'package:valorant/providers/maps_provider.dart';

class MapsRepository extends GetxService {
  final MapsProvider mapsProvider;

  MapsRepository({required this.mapsProvider});

  Future<Response> getAllMaps() async {
    return await mapsProvider.getAllMaps();
  }
}
