import 'package:get/get.dart';
import 'package:valorant/providers/agent_provider.dart';

class AgentRepository extends GetxService {
  final AgentProvider agentProvider;

  AgentRepository({required this.agentProvider});

  Future<Response> getAllAgents() async {
    return await agentProvider.getAllAgents();
  }
}
