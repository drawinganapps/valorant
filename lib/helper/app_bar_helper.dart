import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:valorant/helper/color_helper.dart';

class AppBarHelper {
  static AppBar appBar(context) {
    return AppBar(
      backgroundColor: ColorHelper.dark,
      elevation: 0,
      leadingWidth: 65,
      title: Column(
        children: [
          Image.asset(
            'assets/logo/logo.png',
            width: 70,
            height: 70,
          ),
        ],
      ),
      centerTitle: true,
      leading: Builder(builder: (context) {
        return Container(
          padding: const EdgeInsets.only(left: 15, bottom: 5),
          child: IconButton(
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            icon: Icon(Icons.menu, color: ColorHelper.white, size: 40),
          ),
        );
      }),
    );
  }
}
