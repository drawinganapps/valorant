import 'package:flutter/material.dart';
import 'package:valorant/helper/app_bar_helper.dart';
import 'package:valorant/pages/weapons/weapon_page.dart';
import 'package:valorant/widgets/shared/bottom_navigation_widget.dart';
import 'package:valorant/widgets/shared/side_menu_drawer_widget.dart';

class WeaponScreen extends StatelessWidget {
  const WeaponScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarHelper.appBar(context),
      body: const WeaponPage(),
      drawer: const SideMenuDrawerWidget(),
      bottomNavigationBar: const BottomNavigationWidget(),
    );
  }
}
