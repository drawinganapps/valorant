import 'package:flutter/material.dart';
import 'package:valorant/pages/maps/maps_detail_page.dart';

class MapDetailScreen extends StatelessWidget {
  const MapDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: MapsDetailPage(),
    );
  }

}
