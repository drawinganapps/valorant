import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:valorant/controller/on_boarding_controller.dart';
import 'package:valorant/data/dummy_data.dart';
import 'package:valorant/widgets/onboarding_widget.dart';

class OnBoardingScreen extends StatelessWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OnBoardingController>(builder: (controller) {
      return Scaffold(
        body: OnBoardingWidget(
          onBoarding: DummyData.onBoarding[controller.currentIndex],
        ),
      );
    });
  }
}
