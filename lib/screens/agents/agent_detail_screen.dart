import 'package:flutter/material.dart';
import 'package:valorant/pages/agents/agent_detail_page.dart';

class AgentDetailScreen extends StatelessWidget {
  const AgentDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: AgentDetailPage(),
    );
  }
}
