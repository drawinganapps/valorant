import 'package:flutter/material.dart';
import 'package:valorant/helper/app_bar_helper.dart';
import 'package:valorant/pages/agents/agent_page.dart';
import 'package:valorant/widgets/shared/bottom_navigation_widget.dart';
import 'package:valorant/widgets/shared/side_menu_drawer_widget.dart';

class AgentScreen extends StatelessWidget {
  const AgentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarHelper.appBar(context),
      body: const AgentsPage(),
      drawer: const SideMenuDrawerWidget(),
      bottomNavigationBar: const BottomNavigationWidget(),
    );
  }
}
