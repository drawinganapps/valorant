import 'package:get/get.dart';
import 'package:valorant/models/weapon_category_model.dart';
import 'package:valorant/models/weapon_model.dart';
import 'package:valorant/repository/weapon_repository.dart';

class WeaponController extends GetxController {
  final WeaponRepository weaponRepository;
  List<WeaponModel> _weaponLists = [];
  List<WeaponModel> _filteredWeaponLists = [];
  WeaponCategoryModel _selectedCategory = WeaponCategoryModel(weaponCategoryName: 'Show All', weaponCategoryMachineName: 'all');
  List<WeaponCategoryModel> _weaponCategories = [];
  bool _isDataLoaded = false;
  final _isLoading = false.obs;

  WeaponController({required this.weaponRepository});

  List<WeaponModel> get getAllWeapon => _weaponLists;

  List<WeaponModel> get getFilteredWeapons => _filteredWeaponLists;

  List<WeaponCategoryModel> get getWeaponCategories => _weaponCategories;

  WeaponCategoryModel get getSelectedWeaponCategory => _selectedCategory;

  bool get isLoading => _isLoading.value;

  @override
  void onInit() {
    _fetchAllWeaponList();
    super.onInit();
  }

  Future<void> _fetchAllWeaponList() async {
    _isLoading.value = true;
    if (_isDataLoaded) {
      _isLoading.value = false;
      return;
    }

    Response response = await weaponRepository.getAllWeapons();
    _isDataLoaded = true;
    if (response.statusCode == 200) {
      _initWeaponData(response.body['data']);
      _initWeaponCategory(response.body['categories']);
      _filteredWeaponLists = _weaponLists;
      _selectedCategory = _weaponCategories[0];
      _isLoading.value = false;
      update();
    }
  }

  Future<void> refreshData() async {
    _selectedCategory = WeaponCategoryModel(
        weaponCategoryName: 'Show All', weaponCategoryMachineName: 'all');
    _isDataLoaded = false;
    _weaponLists = [];
    _weaponCategories = [];
    _fetchAllWeaponList();
  }

  void changeSelectedWeaponCategory(WeaponCategoryModel weaponCategory) {
    _selectedCategory = weaponCategory;
    if (weaponCategory.weaponCategoryMachineName == 'all') {
      _filteredWeaponLists = _weaponLists;
    } else {
      _filteredWeaponLists = _weaponLists.where((element) {
        return element.weaponCategoryMachineName! ==
            weaponCategory.weaponCategoryMachineName;
      }).toList();
    }
    update();
  }

  void _initWeaponData(List<dynamic> weaponData) {
    if (weaponData.isNotEmpty) {
      for (var element in weaponData) {
        _weaponLists.add(WeaponModel.fromJson(element));
      }
    }
  }

  void _initWeaponCategory(List<dynamic> weaponCategory) {
    _weaponCategories.add(WeaponCategoryModel(
        weaponCategoryName: 'Show All', weaponCategoryMachineName: 'all'));

    if (weaponCategory.isNotEmpty) {
      for (var element in weaponCategory) {
        _weaponCategories.add(WeaponCategoryModel.fromJson(element));
      }
    }
  }
}
