import 'package:enum_to_string/enum_to_string.dart';
import 'package:get/get.dart';
import 'package:valorant/models/agent_model.dart';
import 'package:valorant/repository/agent_repository.dart';

class AgentController extends GetxController {
  final AgentRepository agentRepository;
  List<AgentModel> _agentList = [];
  late AgentModel _selectedAgent;
  late AgentLocalizationModel _agentLocalization;
  AgentRole _filteredAgentRole = AgentRole.All;
  bool _isDataLoaded = false;
  final _isLoading = false.obs;

  AgentController({required this.agentRepository});

  List<AgentModel> get allAgents => _agentList;

  List<AgentModel> get filteredAgentList {
    if (_filteredAgentRole == AgentRole.All) {
      return _agentList;
    }
    return _agentList
        .where((element) =>
            EnumToString.fromString(AgentRole.values, element.role!) ==
            _filteredAgentRole)
        .toList();
  }

  AgentModel get selectedAgent => _selectedAgent;

  AgentLocalizationModel get agentLocalization => _agentLocalization;

  AgentRole get selectedFilter => _filteredAgentRole;

  bool get isLoading => _isLoading.value;

  @override
  void onInit() {
    _fetchAllAgentList();
    super.onInit();
  }

  Future<void> _fetchAllAgentList() async {
    _isLoading.value = true;
    if (_isDataLoaded) {
      _isLoading.value = false;
      return;
    }

    Response response = await agentRepository.getAllAgents();
    _isDataLoaded = true;
    if (response.statusCode == 200) {
      _initAgentData(response.body['data']);
      _agentLocalization = AgentLocalizationModel.fromJson(response.body);
      _isLoading.value = false;
      update();
    }
  }

  Future<void> refreshData() async {
    _isDataLoaded = false;
    _filteredAgentRole = AgentRole.All;
    _agentList = [];
    _fetchAllAgentList();
  }

  void changeFilterAgent(AgentRole filter) {
    _filteredAgentRole = filter;
    update();
  }

  void setSelectedAgent(AgentModel agent) {
    _selectedAgent = agent;
    update();
  }

  void _initAgentData(List<dynamic> agentData) {
    if (agentData.isNotEmpty) {
      for (var element in agentData) {
        _agentList.add(AgentModel.fromJson(element));
      }
    }
  }
}
