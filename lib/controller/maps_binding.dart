import 'package:get/get.dart';
import 'package:valorant/controller/maps_controller.dart';
import 'package:valorant/providers/maps_provider.dart';
import 'package:valorant/repository/maps_repository.dart';

class MapsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MapsProvider());
    Get.lazyPut(() => MapsRepository(mapsProvider: Get.find()));
    Get.lazyPut<MapsController>(
        () => MapsController(mapsRepository: Get.find()));
  }
}
