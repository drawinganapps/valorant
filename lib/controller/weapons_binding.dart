import 'package:get/get.dart';
import 'package:valorant/controller/weapon_controller.dart';
import 'package:valorant/providers/weapon_provider.dart';
import 'package:valorant/repository/weapon_repository.dart';

class WeaponBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => WeaponProvider());
    Get.lazyPut(() => WeaponRepository(weaponProvider: Get.find()));
    Get.lazyPut<WeaponController>(
        () => WeaponController(weaponRepository: Get.find()));
  }
}
