import 'package:get/get.dart';
import 'package:valorant/models/map_model.dart';
import 'package:valorant/repository/maps_repository.dart';

class MapsController extends GetxController {
  final MapsRepository mapsRepository;
  List<MapModel> _mapsList = [];
  late MapModel _selectedMap;
  bool _isDataLoaded = false;
  final _isLoading = false.obs;

  List<MapModel> get getAllMaps => _mapsList;

  MapModel get selectedMap => _selectedMap;

  bool get isLoading => _isLoading.value;

  MapsController({required this.mapsRepository});

  @override
  void onInit() {
    _fetchAllMapList();
    super.onInit();
  }

  Future<void> _fetchAllMapList() async {
    _isLoading.value = true;
    if (_isDataLoaded) {
      _isLoading.value = false;
      return;
    }

    Response response = await mapsRepository.getAllMaps();
    _isDataLoaded = true;
    if (response.statusCode == 200) {
      _mapsList.addAll(Maps.fromJson(response.body).allMaps);
      _isLoading.value = false;
      update();
    }
  }

  Future<void> refreshData() async {
    _isDataLoaded = false;
    _mapsList = [];
    _fetchAllMapList();
  }

  void setSelectedMap(MapModel map) {
    _selectedMap = map;
    update();
  }
}
