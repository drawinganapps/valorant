import 'package:get/get.dart';
import 'package:valorant/controller/agent_controller.dart';
import 'package:valorant/providers/agent_provider.dart';
import 'package:valorant/repository/agent_repository.dart';

class AgentBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(()=>AgentProvider());
    Get.lazyPut(() => AgentRepository(agentProvider: Get.find()));
    Get.lazyPut<AgentController>(() => AgentController(agentRepository: Get.find()));
  }
}
