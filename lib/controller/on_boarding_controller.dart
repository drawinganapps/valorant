import 'package:get/get.dart';

class OnBoardingController extends GetxController {
  var currentIndex = 0;

  void changeObBoarding(int index)  {
    currentIndex = index;
    update();
  }

  void resetOnBoarding() {
    currentIndex = 0;
    update();
  }
}
