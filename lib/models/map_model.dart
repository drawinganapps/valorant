class Maps {
  late List<MapModel> _maps;

  List<MapModel> get allMaps => _maps;

  Maps({required agents}) {
    _maps = agents;
  }

  Maps.fromJson(Map<String, dynamic> json) {
    final List<dynamic> data = json['data'];
    if (data.isNotEmpty) {
      _maps = [];
      for (var element in data) {
        _maps.add(MapModel.fromJson(element));
      }
    }
  }
}

class MapModel {
  String? mapName;
  String? mapDescription;
  MapFeaturedImageModel? mapFeaturedImage;
  MapFeaturedImageModel? mapFeaturedImageMobile;
  List<GalleryImagesModel>? galleryImages;

  MapModel(
      {this.mapName,
      this.mapDescription,
      this.mapFeaturedImage,
      this.mapFeaturedImageMobile,
      this.galleryImages});

  MapModel.fromJson(Map<String, dynamic> json) {
    mapName = json['map_name'];
    mapDescription = json['map_description'];
    mapFeaturedImage = json['map_featured_image'] != null
        ? MapFeaturedImageModel.fromJson(json['map_featured_image'])
        : null;
    mapFeaturedImageMobile = json['map_featured_image_mobile'] != null
        ? MapFeaturedImageModel.fromJson(json['map_featured_image_mobile'])
        : null;
    if (json['gallery_images'] != null) {
      galleryImages = <GalleryImagesModel>[];
      json['gallery_images'].forEach((v) {
        galleryImages!.add(GalleryImagesModel.fromJson(v));
      });
    }
  }
}

class MapFeaturedImageModel {
  String? url;

  MapFeaturedImageModel({this.url});

  MapFeaturedImageModel.fromJson(Map<String, dynamic> json) {
    url = json['url'];
  }
}

class GalleryImagesModel {
  String? imageDescription;
  bool? isMinimap;
  MapFeaturedImageModel? mapImage;

  GalleryImagesModel({this.imageDescription, this.isMinimap, this.mapImage});

  GalleryImagesModel.fromJson(Map<String, dynamic> json) {
    imageDescription = json['image_description'];
    isMinimap = json['is_minimap'];
    mapImage = json['map_image'] != null
        ? MapFeaturedImageModel.fromJson(json['map_image'])
        : null;
  }
}
