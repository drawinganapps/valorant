class WeaponCategoryModel {
  String? weaponCategoryName;
  String? weaponCategoryMachineName;

  WeaponCategoryModel({this.weaponCategoryName, this.weaponCategoryMachineName});

  WeaponCategoryModel.fromJson(Map<String, dynamic> json) {
    weaponCategoryName = json['weapon_category_name'];
    weaponCategoryMachineName = json['weapon_category_machine_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['weapon_category_name'] = weaponCategoryName;
    data['weapon_category_machine_name'] = weaponCategoryMachineName;
    return data;
  }
}
